#ifndef _APRILTAG
#define _APRILTAG

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>

#include "apriltag/apriltag.h"
#include "apriltag/tag36h11.h"

class tag {
public:
  float x;
  float y;
  float size;
  tag(float x_in = 0, float y_in = 0, float size_in = 0) {
    x = x_in;
    y = y_in;
    size = size_in;
  }
};



class ApriltagDetector {
 public:
  // Apriltag detector
  apriltag_detector_t *tag_det;
  apriltag_family_t *tag_fam;
  zarray_t *detections;
  apriltag_detection_t *det;

  // image variables
  int img_width;
  int img_height;
  cv::Mat img;
  cv::Mat img_gray;
  image_u8_t *img_det;

  // Variables for locationfinding
  std::unordered_map<int, tag> tag_info;
  tag ideal_tag;

  // frame rate record and overlay
  int frame;
  float fps;
  char fps_text[10];
  double last_t;
  int font;
  double font_scale;
  int thickness;
  cv::Point fps_origin;

  // Control logic
  bool print_tags;

  // Default Constructor
  ApriltagDetector(int new_width = 1280, int new_height = 720) :
  print_tags(false),
  ideal_tag(0, 0, .10),
  frame(0),
  fps(0),
  last_t(0)
  {
    img_width = new_width;
    img_height = new_height;

    // Initialize Apriltag objects
    tag_det = apriltag_detector_create();
    tag_fam = tag36h11_create();
    apriltag_detector_add_family(tag_det, tag_fam);
    detections = zarray_create(sizeof(apriltag_detection_t*));

    // Preallocate image matrices
    img = cv::Mat(img_height, img_width, CV_8UC3, cv::Scalar(0,0,0));
    img_gray = cv::Mat(img_height, img_width, CV_8U, 0);
    img_det = image_u8_create(img_width, img_height);

    font = cv::FONT_HERSHEY_SIMPLEX;
    font_scale = 1;
    thickness = 2;
    cv::Size text_size = cv::getTextSize(fps_text, font, font_scale,
      thickness, 0);
    fps_origin = cv::Point(10, img_height-10);
  }

  ~ApriltagDetector(void) {
    apriltag_detector_destroy(tag_det);
    tag36h11_destroy(tag_fam);
    zarray_destroy(detections);
    apriltag_detection_destroy(det);
    image_u8_destroy(img_det);
  }

  // Load tag location and sizes from text file
  void load_tags(std::string f_id);

  // Detect apriltags in color or gray opencv image
  void detect_tags(const cv::Mat& img_in) {
    assert((img_in.rows == img_height) && (img_in.cols == img_width));

    if (img_in.channels() == 3) {
      cv::cvtColor(img_in, img_gray, CV_BGR2GRAY);
      cvimage_to_imageu8(img_gray, img_det);
    }
    else
      cvimage_to_imageu8(img_in, img_det);

    zarray_destroy(detections);
    detections = apriltag_detector_detect(tag_det, img_det);
    calc_fps();
  }

  // Convert opencv mat to image_u8_t
  void cvimage_to_imageu8(cv::Mat img_in, image_u8_t *img_out);

  // Convert matd to cv::Mat
  cv::Mat matd_to_opencv(const matd_t *mat);
  
  // Overlay detections onto image
  void draw(cv::Mat img_in, bool draw_fps = true);

  // Compute relative position using homography
  cv::Mat compute_position(void); // use last calculated detections
  cv::Mat compute_position(const cv::Mat& img_in); // do new detection from image

  // Updates fps
  void calc_fps(void) {
    if (++frame % 5 == 0) {
      fps = 5.0/(cv_tic() - last_t);
      sprintf(fps_text, "%04.2f FPS", fps);
      last_t = cv_tic();
    }
  }

  // LOOPING FUNCTION FOR CONTINUOUS ACQUISITION WITH OPENCV
  // void demo_loop(void) {
  //   cv::Mat pose(4,4,CV_64F);

  //   char fps_text[10];
  //   sprintf(fps_text, "%04.2f FPS", 10.0);
  //   int fontFace = cv::FONT_HERSHEY_SIMPLEX;
  //   double fontScale = 1;
  //   int thickness = 2;
  //   cv::Size text_size = cv::getTextSize(fps_text, fontFace, fontScale,
  //     thickness, 0);
  //   cv::Point text_org((img_width - text_size.width-10), img_height-10);

  //   double t = tic();
  //   int frame = 0;
  //   while (true) {
  //     cap >> image_cap;
  //     // detect_tags();

  //     if (++frame % 10 == 0) {
  //     sprintf(fps_text, "%04.2f FPS", 10.0/(tic() - t));
  //     frame = 0;
  //     t = tic();
  //     // std::cout << "H = "<< std::endl << " "  << pose << std::endl << std::endl;
  //     }

  //     if (display_images) {
  //       cv::putText(image_cap, fps_text, text_org, fontFace, fontScale,
  //          cv::Scalar(0, 255, 0), thickness, 8);
  //       // draw(image_cap, detections);
  //       // pose = compute_position(detections);
  //       cv::imshow(window_name, image_cap);
  //       if (cv::waitKey(1) == 27)
  //         break;
  //     }
  //   }

  //   cv::destroyWindow(window_name);
  // }
  double cv_tic(void) {
    return cv::getTickCount()/cv::getTickFrequency();
  }
};



#endif 
/* _APRILTAG */
