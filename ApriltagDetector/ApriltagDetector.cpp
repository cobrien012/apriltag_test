#include "ApriltagDetector.h"

void ApriltagDetector::load_tags(std::string f_id) {
  std::ifstream infile(f_id);

  int tag_id;
  float x, y, size;

  std::string line;
  std::stringstream ss;
  while (std::getline(infile, line)) {
    if (line[0] != '#') {
      ss.str(line);
      ss.clear();
      ss >> tag_id
        >> x >> y
        >> size;
      std::cout << "Tag ID:" << tag_id
      << ", X: " << x
      << ", Y: " << y
      << ", Size: " << size << std::endl;

      if (!tag_info.count(tag_id))
        tag_info.insert({tag_id, tag(x,y,size)});
      else
        std::cout << "Tag already specified! Check config file" << std::endl;
    }
  }
}

void ApriltagDetector::cvimage_to_imageu8(cv::Mat img_in, image_u8_t *img_out) {
  // Copy by rows, image_u8_t is rarely contiguous, so use array stride for row pointer
  uchar* p;
  for (int row = 0; row < img_in.rows; ++row) {
    p = img_in.ptr<uchar>(row);
    memcpy(&img_out->buf[row*img_out->stride], p, img_in.cols*sizeof(uint8_t));
  }
}

cv::Mat ApriltagDetector::matd_to_opencv(const matd_t *mat) {
  return cv::Mat(mat->nrows, mat->ncols, CV_64F, *mat->data).clone();
}

void ApriltagDetector::draw(cv::Mat img_in, bool draw_fps) {
  for (int i = 0; i < zarray_size(detections); i++) {
    apriltag_detection_t *det;
    zarray_get(detections, i, &det);

    cv::putText(img_in, std::to_string(det->id),
      cv::Point(det->c[0]+5, det->c[1]+5),
      cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 0, 255), 2, 8, false);

    cv::Point center(det->c[0], det->c[1]);
    cv::Point p0(det->p[0][0], det->p[0][1]);
    cv::Point p1(det->p[1][0], det->p[1][1]);
    cv::Point p2(det->p[2][0], det->p[2][1]);
    cv::Point p3(det->p[3][0], det->p[3][1]);

    cv::line(img_in, p0, p1, cv::Scalar(255, 0, 255), 2);
    cv::line(img_in, p1, p2, cv::Scalar(255, 0, 255), 2);
    cv::line(img_in, p2, p3, cv::Scalar(255, 0, 255), 2);
    cv::line(img_in, p3, p0, cv::Scalar(255, 0, 255), 2);

    int radius = 3;
    int thickness = -1;

    cv::circle(img_in, center, radius, cv::Scalar(0, 0, 255), thickness);
    cv::circle(img_in, p0, radius, cv::Scalar(255,   0,   0), thickness);
    cv::circle(img_in, p1, radius, cv::Scalar(  0, 255,   0), thickness);
    cv::circle(img_in, p2, radius, cv::Scalar(255, 255,   0), thickness);
    cv::circle(img_in, p3, radius, cv::Scalar(  0, 255, 255), thickness);
  }
  if (draw_fps)
  cv::putText(img_in, ApriltagDetector::fps_text, ApriltagDetector::fps_origin,
    ApriltagDetector::font, ApriltagDetector::font_scale, cv::Scalar(0, 255, 0),
    ApriltagDetector::thickness, 8);
}

cv::Mat ApriltagDetector::compute_position(const cv::Mat& img_in) {
  ApriltagDetector::detect_tags(img_in);
  return ApriltagDetector::compute_position();
}

cv::Mat ApriltagDetector::compute_position(void) {
  if (zarray_size(detections) == 0)
    return cv::Mat::zeros(4, 4, CV_64F);

  tag det_tag;

  // correspondences is a zarray list of float[4], where each element is {a,b,c,d},
  // such that y = Hx, x = [a,b], y = [c d]
  float corr_point[4];
  zarray_t *correspondences = zarray_create(sizeof(float[4]));

  for (int tag = 0; tag < detections->size; tag++) {
    zarray_get(detections, tag, &det);

    // If no tags have been specified, use ideal and skip rest
    if (tag_info.empty()) {
      det_tag = ideal_tag;
      tag = detections->size;
    }

    // If tags are specified, but this one isnt in map, skip and notify
    else if (!tag_info.count(det->id)) { 
      std::cout << "Unknown Tag! Skipping it!" << std::endl;
      continue;
    }

    // else load appropriate tag from map
    else
      det_tag = tag_info[det->id];

    // add center point
    corr_point[0] = det_tag.x;
    corr_point[1] = det_tag.y;
    corr_point[2] = det->c[0];
    corr_point[3] = det->c[1];
    zarray_add(correspondences, &corr_point);

    // add corner points
    for (int i = 0; i < 4; i++) {
      corr_point[0] = det_tag.x + ((i == 0 || i == 3) ? -det_tag.size/2 : det_tag.size/2);
      corr_point[1] = det_tag.y + ((i == 0 || i == 1) ? -det_tag.size/2 : det_tag.size/2);
      corr_point[2] = det->p[i][0];
      corr_point[3] = det->p[i][0];
      zarray_add(correspondences, &corr_point);
    }
  }

  matd_t *H = homography_compute(correspondences, 0);
  matd_t *pose = homography_to_pose(H, 600, 600, img_width/2.0, img_height/2.0);

  cv::Mat pose_cv = matd_to_opencv(pose);

  matd_destroy(H);
  matd_destroy(pose);

  return pose_cv;
}


