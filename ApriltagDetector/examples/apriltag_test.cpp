#include "../ApriltagDetector.h"

// using namespace ApriltagDetector;

int main(int argc, char** argv) {

  int w = 1280/2;
  int h = 720/2;
  ApriltagDetector at(w, h);

  cv::namedWindow("TEST");
  cv::Mat img(h, w, CV_8UC3, cv::Scalar(0,0,0));
  cv::Mat gray(h, w, CV_8U, 0);
  // cv::VideoCapture cap(-1);
  cv::VideoCapture cap( CV_CAP_OPENNI2 );
  cap.set(CV_CAP_PROP_FRAME_WIDTH,w);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,h);
  cap >> img;
  cvtColor(img, gray, CV_BGR2GRAY);

  std::cout << "opened" << std::endl;

  while (true) {
    cap >> img;
    cvtColor(img, gray, CV_BGR2GRAY);
    at.compute_position(gray);
    at.draw(img);
    cv::imshow("TEST", img);
    if (cv::waitKey(1) == 27)
      break;
  }

  return 0;
}
